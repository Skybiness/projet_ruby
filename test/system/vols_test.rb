require "application_system_test_case"

class VolsTest < ApplicationSystemTestCase
  setup do
    @vol = vols(:one)
  end

  test "visiting the index" do
    visit vols_url
    assert_selector "h1", text: "Vols"
  end

  test "creating a Vol" do
    visit vols_url
    click_on "New Vol"

    fill_in "Aeroport arr", with: @vol.aeroport_arr
    fill_in "Aeroport dep", with: @vol.aeroport_dep
    fill_in "Date heure arr", with: @vol.date_heure_arr
    fill_in "Date heure dep", with: @vol.date_heure_dep
    fill_in "Numero", with: @vol.numero
    click_on "Create Vol"

    assert_text "Vol was successfully created"
    click_on "Back"
  end

  test "updating a Vol" do
    visit vols_url
    click_on "Edit", match: :first

    fill_in "Aeroport arr", with: @vol.aeroport_arr
    fill_in "Aeroport dep", with: @vol.aeroport_dep
    fill_in "Date heure arr", with: @vol.date_heure_arr
    fill_in "Date heure dep", with: @vol.date_heure_dep
    fill_in "Numero", with: @vol.numero
    click_on "Update Vol"

    assert_text "Vol was successfully updated"
    click_on "Back"
  end

  test "destroying a Vol" do
    visit vols_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Vol was successfully destroyed"
  end
end
