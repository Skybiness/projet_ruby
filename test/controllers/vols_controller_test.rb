require "test_helper"

class VolsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @vol = vols(:one)
  end

  test "should get index" do
    get vols_url
    assert_response :success
  end

  test "should get new" do
    get new_vol_url
    assert_response :success
  end

  test "should create vol" do
    assert_difference('Vol.count') do
      post vols_url, params: { vol: { aeroport_arr: @vol.aeroport_arr, aeroport_dep: @vol.aeroport_dep, date_heure_arr: @vol.date_heure_arr, date_heure_dep: @vol.date_heure_dep, numero: @vol.numero } }
    end

    assert_redirected_to vol_url(Vol.last)
  end

  test "should show vol" do
    get vol_url(@vol)
    assert_response :success
  end

  test "should get edit" do
    get edit_vol_url(@vol)
    assert_response :success
  end

  test "should update vol" do
    patch vol_url(@vol), params: { vol: { aeroport_arr: @vol.aeroport_arr, aeroport_dep: @vol.aeroport_dep, date_heure_arr: @vol.date_heure_arr, date_heure_dep: @vol.date_heure_dep, numero: @vol.numero } }
    assert_redirected_to vol_url(@vol)
  end

  test "should destroy vol" do
    assert_difference('Vol.count', -1) do
      delete vol_url(@vol)
    end

    assert_redirected_to vols_url
  end
end
