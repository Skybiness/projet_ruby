class ReservationMailer < ApplicationMailer

  def send_reservation_mail(user, reservation)
    @user = user
    @reservation = reservation
    mail(to: @user.email, from: "root@localhost", subject: 'Email d\'envoie de code de réservation')
  end
end
