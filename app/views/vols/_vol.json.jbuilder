json.extract! vol, :id, :numero, :aeroport_dep, :aeroport_arr, :date_heure_dep, :date_heure_arr, :created_at, :updated_at
json.url vol_url(vol, format: :json)
