json.extract! reservation, :id, :classe, :code, :nb_passager, :created_at, :updated_at
json.url reservation_url(reservation, format: :json)
