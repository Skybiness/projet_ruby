# README

Pour initialiser le projet : 

    - Bundle install
    - yarn add webpack

Initialisation de la base de donnée : 

    - rails db:migrate

Initialisation la gestion des mails : 

    - rails gem install mailcatcher
    - mailcatcher ( pour recevoir les mails )

Les potentielles bug :

    Pour visualiser la base de données : 

        - clique sur la bdd à droite 
        - clique doit sur développement ( la bdd )
        - clique sur properties File: et changer le chemin d'accès de la bdd

