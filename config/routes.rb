Rails.application.routes.draw do

  resources :reservations
  resources :vols

  devise_for :users

  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
  root "home#index"

  get "vol/vol"
  get 'home/private'
end
