class CreateVols < ActiveRecord::Migration[6.1]
  def change
    create_table :vols do |t|
      t.string :numero
      t.string :aeroport_dep
      t.string :aeroport_arr
      t.datetime :date_heure_dep
      t.datetime :date_heure_arr

      t.timestamps
    end
  end
end
